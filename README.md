# price_tracker_api
An API for tracking prices of different items in different supermarkets

| Endpoint | Functionality | Note |
| -------- | ------------- | ---- |
| POST /auth/signup | Register a user | Public Access |
| POST /auth/login | Login a user | Public Access |
| POST /supermarkets | Add a supermarket | Requires approval from admin if created by regular user |
| GET /supermarkets | Fetch all supermarkets | Require login |
| GET /supermarkets/<`supermarket_id`> | Fetch a single supermarket | Require login |  
| GET /supermarkets/<`supermarket_id`>/items | Fetch all items in a given supermarket | Require login |
| GET /supermarkets/<`supermarket_id`>/items/<`item_id`> | Fetch a particular item in a given supermarket | Require login |
| POST /supermarkets/<`supermarket_id`>/items | Add an item to a particular supermarket | Require login |
