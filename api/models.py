from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class TimeStampedModel(models.Model):
    """
    Abstract base class model that provides
    self-updating `created` and `modified`
    fields
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Supermarket(TimeStampedModel):
    name = models.CharField(max_length=100, unique=True, help_text='Name of the supermarket')
    location = models.CharField(max_length=100, unique=True, help_text='Location of the supermarket')
    creator = models.ForeignKey(User)

    def get_absolute_url(self):
        return reverse('supermarkets', args=[str(self.id)])

    class Meta:
        ordering = ['name']
        get_latest_by = [-'modified', 'created']


class Item(TimeStampedModel):
    name = models.CharField(max_length=100, unique=True,help_text='Name of the Item')
    description = models.CharField(max_length=4000, help_text='Description of the Item')
    price = models.IntegerField(default=0, help_text='Price of the Item')
    creator = models.ForeignKey(User)
    supermarket = models.ForeignKey(Supermarket, on_delete=models.PROTECT)

    def get_absolute_url(self):
        return reverse('supermarkets', args=[str(self.id)])

    class Meta:
        ordering = ['name']
        get_latest_by = [-'modified', 'created']
