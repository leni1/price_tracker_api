FROM python:3

WORKDIR /app

RUN addgroup --system django \
    && adduser --system --ingroup django django

COPY requirements.txt ./

# Install pip packages
RUN pip install --no-cache-dir -r requirements.txt

COPY --chown=django:django . /app

USER django

WORKDIR /app

# Expose required ports
EXPOSE 8000
